package practica.umg;

/**
 * Created by INE on 4/07/2017.
 */
public class Asignatura {
    private String nombre;
    private int nota;
    private int cantEvaluacion;

    public Asignatura(String nombre, int nota, int cantEvaluacion) {
        this.nombre = nombre;
        this.nota = nota;
        this.cantEvaluacion = cantEvaluacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }

    public int getCantEvaluacion() {
        return cantEvaluacion;
    }

    public void setCantEvaluacion(int cantEvaluacion) {
        this.cantEvaluacion = cantEvaluacion;
    }
}
