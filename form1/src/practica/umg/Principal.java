package practica.umg;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by INE on 4/07/2017.
 */
public class Principal extends JDialog {
    private JPanel contentPanel;
    private JMenu manuArchivo = new JMenu("Archivo");
    private JMenuItem nuevo = new JMenuItem("Nuevo");
    private JMenuItem menuItemSalir = new JMenuItem("Salir");
    private JMenuBar menuBar = new JMenuBar();
    private JMenu menuProfesor = new JMenu("Profesor");
    private JMenuItem addProfesor = new JMenuItem("Agregar Nuevo Profesor");
    private JMenuItem estDestacado = new JMenuItem("Estudiante Destacado");
    private JPanel panelContenido;
    private JTextField txtNombre;
    private JTextField txtEdad;
    private JLabel lblNombreP;
    private JLabel lblEspecialidadP;
    private JTextField txtAsignatura;
    private JTextField txtNota;
    private JTextField txtEvaluaciones;
    private JButton btnAdd;
    private JButton btnDel;
    private JTable tblEstudiantes;
    private DefaultTableModel modelo;
    Profesor p= new Profesor();


    public Principal() {
        setContentPane(contentPanel);
        setJMenuBar(menuBar);
        menuBar.add(manuArchivo);
        manuArchivo.add(nuevo);
        manuArchivo.add(menuItemSalir);
        menuBar.add(menuProfesor);
        menuProfesor.add(addProfesor);
        menuProfesor.add(estDestacado);
        setModal(true);
        modelo = new DefaultTableModel();
        modelo.addColumn("Nombre Estudiante");
        modelo.addColumn("Edad Estudiante");
        modelo.addColumn("Curso/Asignatura");
        modelo.addColumn("Nota");
        modelo.addColumn("Cant. Evaluaciones");
        tblEstudiantes.setModel(modelo);

        menuItemSalir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        addProfesor.addActionListener(new ActionListener(){
                    public void actionPerformed(ActionEvent e) {
                        String strNombre = JOptionPane.showInputDialog("Catedratico", "");
                        String strEspecialidad = JOptionPane.showInputDialog("Especialidad", "");
                        p.setNombre(strNombre);
                        p.setEspecialidad(strEspecialidad);
                        lblNombreP.setText(p.getNombre());
                        lblEspecialidadP.setText(p.getEspecialidad());
                        addProfesor.setEnabled(false);
            }
        });
        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onAdd();
            }
        });
        btnDel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onDelete();
            }
        });
        estDestacado.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(contentPanel, p.estudianteMasDestacado().getNombre(), "Estudiante con el mejor promedio", JOptionPane.INFORMATION_MESSAGE);
                System.out.println(p.estudianteMasDestacado().getNombre());
            }
        });
    }

    public static void main(String[] args) {
        Principal pri = new Principal();
        pri.pack();
        pri.setVisible(true);
        System.exit(0);
    }

    private void onAdd() {
        if ((txtNombre.getText().length()==0) || (txtEdad.getText().length()==0) || (txtAsignatura.getText().length()==0) || (txtEvaluaciones.getText().length()==0) || (txtNota.getText().length()==0)) {
            JOptionPane.showMessageDialog(contentPanel,"Todos los campos  son Obligatorios", "ERROR", JOptionPane.ERROR_MESSAGE);
        } else {
            Estudiante est = new Estudiante();
            est.setNombre(txtNombre.getText());
            est.setEdad(Integer.parseInt(txtEdad.getText()));
            est.addAsignatura(new Asignatura(txtAsignatura.getText(), Integer.parseInt(txtNota.getText()), Integer.parseInt(txtEvaluaciones.getText())));
            modelo.addRow(new Object[] {est.getNombre(), est.getEdad(), txtAsignatura.getText(), txtNota.getText(), txtEvaluaciones.getText()});
            JOptionPane.showMessageDialog(contentPanel, "El Alumno fue agregado a la lista exitosamente...", "INGRESO DE DATOS", JOptionPane.INFORMATION_MESSAGE);
            clearFields();
        }
    }

    private void clearFields(){
        txtAsignatura.setText("");
        txtEdad.setText("");
        txtEvaluaciones.setText("");
        txtNombre.setText("");
        txtNota.setText("");
    }

    private void onDelete() {
        modelo.removeRow(tblEstudiantes.getSelectedRow());
        JOptionPane.showMessageDialog(contentPanel, "El Alumno fue eliminado ", "ELIMINADO", JOptionPane.INFORMATION_MESSAGE);
    }
}
